import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

function Node(data) {
    this.w = data.w;
    this.h = data.h;
    this.next = null;
}

function SinglyList() {
    this._length = 0;
    this.head = null;
}

SinglyList.prototype.add = function(value) {
    var node = new Node(value),
        currentNode = this.head;

    // 1st use-case: an empty list
    if (!currentNode) {
        this.head = node;
        this._length++;

        return node;
    }

    // 2nd use-case: a non-empty list
    while (currentNode.next) {
        currentNode = currentNode.next;
    }

    currentNode.next = node;

    this._length++;

    return node;
};

SinglyList.prototype.searchNodeAt = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 1,
        message = {failure: 'Failure: non-existent node in this list.'};

    // 1st use-case: an invalid position
    if (length === 0 || position < 1 || position > length) {
        throw new Error(message.failure);
    }

    // 2nd use-case: a valid position
    while (count < position) {
        currentNode = currentNode.next;
        count++;
    }

    return currentNode;
};

SinglyList.prototype.remove = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 0,
        message = {failure: 'Failure: non-existent node in this list.'},
        beforeNodeToDelete = null,
        nodeToDelete = null,
        deletedNode = null;

    // 1st use-case: an invalid position
    if (position < 0 || position > length) {
        throw new Error(message.failure);
    }

    // 2nd use-case: the first node is removed
    if (position === 1) {
        this.head = currentNode.next;
        deletedNode = currentNode;
        currentNode = null;
        this._length--;

        return deletedNode;
    }

    // 3rd use-case: any other node is removed
    while (count < position) {
        beforeNodeToDelete = currentNode;
        nodeToDelete = currentNode.next;
        count++;
    }

    beforeNodeToDelete.next = nodeToDelete.next;
    deletedNode = nodeToDelete;
    nodeToDelete = null;
    this._length--;

    return deletedNode;
};

SinglyList.prototype.move = function() {
  // if(this.head.w >= $('.snake-body').outerWidth || this.head.w <=0 || this.head.h >= $('.snake-body').outerHeight || this.head.h <=0) {
  //   alert("game over");
  //   return;
  // }
  width_increment = 0
  height_increment = 0
  x = 30
  y = 20
  if(direction == 'r')
    width_increment = x;
  else if( direction == 'l')
    width_increment == -x;
  else if( direction == 'u')
    height_increment = y;
  else if( direction == 'd')
    height_increment = -y

  old_head_w = this.head.w
  old_head_h = this.head.h
  this.head.w = this.head.w + width_increment
  this.head.h = this.head.h + height_increment

  currentNode = list.head
   while(currentNode.next)
   {
     old_w = currentNode.next.w
     old_h = currentNode.next.h
     currentNode.next.w = old_head_w
     currentNode.next.h = old_head_h
     old_head_w = old_w
     old_head_h = old_h
     currentNode = currentNode.next
   }

    draw_snake();

    return currentNode;
};

function draw_snake(){
  var snake_node = list.head
  $('.snake-body').html('');
  while(snake_node){
    $('.snake-body').append('<div class="snake" style="left:'+snake_node.w+'px;top: '+snake_node.h+'px;"></div>')
    snake_node = snake_node.next;
  }
}

list = new SinglyList();

list.add({w: 0, h: 20});
list.add({w: 30, h: 20});
list.add({w: 60, h: 20});

direction = 'r';

Template.hello.rendered = function helloOnCreated() {
  draw_snake();
  setInterval(function(){
    list.move();
  }, 500)
};

Template.hello.helpers({
  // move() {
  //   if(list.head.w >= window.outerWidth || list.head.w <=0 || list.head.h >= window.outerHeight || list.head.h <=0) {
  //     alert("game over");
  //     return;
  //   }
  //   width_increment = 0
  //   height_increment = 0
  //   if direction == 'r'
  //     width_increment = x;
  //   else if direction == 'l'
  //     width_increment == -x;
  //   else if direction == 'u'
  //     height_increment = y;
  //   else if direction == 'd'
  //     height_increment = -y
  //
  //   new_head = { w: list.head.w + width_increment
  //                h: list.head.h + height_increment
  //                next: list.head
  //    }
  //    list.head = new_head
  //    current_node = list.head
  //    temp_list = list.head
  //    while(current_node)
  //    {
  //      temp_list.next = temp_list.next.next
  //      current_node = current_node.next
  //    }
  //
  //
  // },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
